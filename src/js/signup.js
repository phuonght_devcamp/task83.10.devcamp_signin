
    $(document).ready(function () {
        // Lưu một biến token để dùng khi gọi chức năng get user info 
        var gToken = "";

        $("#signup").on("click", function() {
            getUserData();
            signUpFunction();
        });

        // $("#signin").on("click", function() {
        //     signInFunction();
        // });

        // $("#info").on("click", function() {
        //     infoFunction();
        // });

        var gSignUpObj = {
            firstname: '',
            lastname: '',
            email: '',
            password: '',
            reTypePassword:''
        }

        function getUserData(){ 
            gSignUpObj.firstname=$('#firstname').val().trim();
            gSignUpObj.lastname=$('#lastname').val().trim();
            gSignUpObj.email=$('#email').val().trim();
            gSignUpObj.password=$('#password').val().trim();
            gSignUpObj.reTypePassword=$('#retype_password').val().trim();
           
        }
         // Hàm validate email bằng regex
        function validateEmail(email) { 
            const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return regex.test(String(email).toLowerCase());
        }
        //Validate dữ liệu từ form
        function validateForm(gSignUpObj) {
            if(!validateEmail(gSignUpObj.email)) {
                alert("Email không phù hợp");
                return false;
            };

            if(gSignUpObj.password === "") {
                alert("Password không phù hợp");
                return false;
            }
            if(gSignUpObj.firstname === "") {
                alert("firstname không phù hợp");
                return false;
            }
            if(gSignUpObj.lastname === "") {
                alert("lastname không phù hợp");
                return false;
            }
            if(gSignUpObj.reTypePassword !==  gSignUpObj.password) {
                alert("Password nhập vào không trùng nhau");
                return false;
            }
            return true;
        }

        function signUpFunction() { 
           
           var vSignUpRequest = {}
           vSignUpRequest.firstname = gSignUpObj.firstname
           vSignUpRequest.lastname = gSignUpObj.lastname
           vSignUpRequest.email = gSignUpObj.email
           vSignUpRequest.password = gSignUpObj.password
           var vIsdataValid = validateForm(gSignUpObj);
           if(vIsdataValid )
            {   
               
                $.ajax({
                    url: "http://42.115.221.44:8080/devcamp-auth/users/signup",
                    type: 'POST',
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(vSignUpRequest),
                    success: function (pRes) {
                        console.log(pRes)
                    },
                    error: function (pAjaxContext) {
                        console.log(pAjaxContext.responseJSON.message);
                    }
                });
            }
      }
        // function signInFunction() {
        //     var vLoginData = {
        //         email: "minhvd@devcamp.edu.vn",
        //         password: "12345678"
        //     }

        //     $.ajax({
        //         url: "http://42.115.221.44:8080/devcamp-auth/users/signin?email=" + vLoginData.email + '&password=' + vLoginData.password,
        //         type: 'POST',
        //         dataType: "json",
        //         contentType: "application/json; charset=utf-8",
        //         success: function (pRes) {
        //             console.log(pRes);

        //             gToken = pRes.token;
        //         },
        //         error: function (pAjaxContext) {
        //             console.log(pAjaxContext.responseText);
        //         }
        //     });
        // }

        // function infoFunction() {
        //     if(gToken == "") {
        //         alert("Chưa có token");
        //         return;
        //     }

        //     $.ajax({
        //         url: "http://42.115.221.44:8080/devcamp-auth/users/me",
        //         type: 'GET',
        //         dataType: "json",
        //         contentType: "application/json; charset=utf-8",
        //         headers: {
        //             "Authorization": "Bearer " + gToken
        //         },
        //         success: function (pRes) {
        //             console.log(pRes);
        //         },
        //         error: function (pAjaxContext) {
        //             console.log(pAjaxContext.responseText);
        //         }
        //     });
        // }
    })
